﻿param 
( 
    [Parameter(Mandatory = $true, HelpMessage="Enter a PC name")] 
    [ValidateNotNullOrEmpty()] 
    [string]$PCName , 

    [Parameter(Mandatory = $true, HelpMessage="Enter a username")] 
    [ValidateNotNullOrEmpty()] 
    [string]$User = ""
) 

#End ndClick process
$ProcessActive = Get-Process -ComputerName $PCName ndClickWinTray -ErrorAction SilentlyContinue
if($ProcessActive -eq $null) {
 Write-host "ndClick is not running"

} else {
 Write-Host "Stopping ndClick"
 $Process = Get-WmiObject -Class Win32_Process -ComputerName $PCName -Filter "name='ndClickWintray.exe'"
 Write-host "*************"
 $Process.terminate()
 Write-host "*************"
}

#Rename "ND Office Echo" folder to "ND Office Echo - old"
$localpath = "\\$PCName\c$\Users\$User\ND Office Echo"
$newpath = "\\$PCName\c$\Users\$User\ND Office Echo(rollback).old"

If (Test-Path $localpath){
    If(-Not (Test-Path $newpath)){
        Rename-Item $localpath $newpath
        Write-Host "Renamed ND Office Echo successfully."
    }
}