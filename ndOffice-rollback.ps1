#Script used for rolling back ndOffice installation. It grabs the logged in user's username, closes the ndClick application and 
#renames the ND Office Echo folder. It then starts ndOffice and ndClick applications.
#Author: Mohammed Bajaman
#Date: 13th June 2019

#Store domain/username from whoami command
$user = whoami
Write-Host "User: $user"

#Use regex to parse domain\username string and store in variable
$reguser = [regex]::match($user, '\\(.*)$').Groups[1].Value
Write-Host "Regex parsed user: $reguser"

#End ndClick process
$ProcessActive = Get-Process ndClickWinTray -ErrorAction SilentlyContinue
if($ProcessActive -eq $null){
 Write-host "ndClick is not running"
} else {
 Write-Host "Stopping ndClick"
 Stop-Process -Name ndClickWinTray
}

#Rename "ND Office Echo" folder to "ND Office Echo - old"
$localpath = "C:\Users\$reguser\ND Office Echo"
$newpath = "C:\Users\$reguser\ND Office Echo - old"
If (Test-Path $localpath){
    If(-Not (Test-Path $newpath)){
        Rename-Item $localpath $newpath
        Write-Host "Renamed ND Office Echo successfully."
    }
}


