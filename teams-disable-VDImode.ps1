﻿#Author: Mohammed Bajaman
#Date: 2nd June 2021 (02-06-2021)
#Description: Script for deleting Teams config files and copies in the storage.json file from a Windows device where vdiModeEnabled is set to 0 (false)
#The Teams files are located in the user's app data folder and required Teams to be closed before the deletion and paste can take place

$username = $env:USERNAME
$teams_path = "C:\Users\$username\AppData\Roaming\Microsoft\Teams"
$teams_app = Get-Process teams -ErrorAction SilentlyContinue
$validation_path = "C:\teams_breakout_enabled.log"

if(-Not (Test-Path $validation_path)){
    #Close out of teams
    if ($teams_app) {
        $teams_app | Stop-Process -Force
      }

    #Wait for lockfile to go away
    Start-Sleep -Seconds 3

    #Remove files in Teams directory
    Get-ChildItem $teams_path | Where { ! $_.PSIsContainer } | Foreach-Object {Remove-Item $_.FullName}

    #Copy storage.json to Teams directory
    Copy-Item "\\hgfile1\IT\Repo\Microsoft\Microsoft Teams\VDI Mode fix\storage.json" -destination $teams_path

    #Create file to mark device as updated
    New-Item -Path "C:\" -Name "teams_breakout_enabled.log" -ItemType "file" -Value "VDI mode disabled successfully"

    #Launch Teams
    Start-Process -FilePath "C:\Users\$username\AppData\Local\Microsoft\Teams\current\Teams.exe"
} else {
    Write-Host "Teams already has breakout rooms enabled!"
}
