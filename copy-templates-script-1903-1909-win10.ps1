#####Enable File Explorer Naviagtion Pane Settings and Show File Extensions#####
$key = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
Set-ItemProperty $key NavPaneExpandToCurrentFolder 1
Set-ItemProperty $key NavPaneShowAllFolders 1
Set-ItemProperty $key HideFileExt 0
Write-Host "Set File Explorer settings" -ForegroundColor Green

#####Disable auto logon#####
$key = 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon'
Set-ItemProperty $key AutoAdminLogon 0
Write-Host "Disabled AutoAdminLogon" -ForegroundColor Green

#####Delete Unattend files#####
$unattend = @('C:\Windows\Panther\unattend.xml', 'C:\Windows\Panther\Unattend\unattend.xml', 'C:\unattend.xml', 'C:\unattend-capture.xml')
$unattend | ForEach-Object {if(Test-Path "$PSItem"){ Remove-Item -Path "$PSItem"}}
Write-Host "Deleted Unattend files" -ForegroundColor Green

#####Enable Windows Features - .NET Framework 3.5#####
DISM /Online /Enable-Feature /FeatureName:NetFx3 /All /LimitAccess /Source:\\st-dfs\IT\Scripting\1903


$exit = Read-Host -Prompt 'Press enter to exit...';

Stop-Process -processname explorer
