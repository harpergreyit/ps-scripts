﻿<#
Author: Mohammed Bajaman
Date: 30th July 2019
Description: This script edits the dial up connection file located in 
C:\ProgramData\Microsoft\Network\Connections\Pbk\ and overwrites the line
with entry "IPDnsFlags=0" to "IPDnsFlags=1". This is essentially the same as
opening the VPN connection from "Change adapter settings"  > IPv4 properties >
Advanced and checking the "Register this connection's addresses in DNS" box

This workaround was setup for the Airwatch VPN deployed via MDM profiles.
The bulit-in powershell commands do not work when attempting to turn on that
settings.
#>
$file = "C:\ProgramData\Microsoft\Network\Connections\Pbk\rasphone.pbk"

$content = Get-Content -Path $file

for($i=0; $i -lt $content.Length; $i++){
    if($content[$i] -eq "IPDnsFlags=0"){
        Write-Host "Current setting: " + $content[$i]
        $content[$i] ="IPDnsFlags=1"
        Write-Host "`nUpdated setting: " + $content[$i]
    }
}
for($i=0; $i -lt $content.Length; $i++){
    if($content[$i] -eq "IpInterfaceMetric=0"){
        Write-Host "Current setting: " + $content[$i]
        $content[$i] ="IpInterfaceMetric=15"
        Write-Host "`nUpdated setting: " + $content[$i]
    }
}
Write-Host "`nUpdating file..."
$content | Set-Content -Path $file

Write-Host "`nDDNS has been enabled!"

