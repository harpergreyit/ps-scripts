#Author: Mohammed Bajaman
#Date: 25th November 2019
#Description: Script to restart NetDocuments services on a computer

#End all ND processes
Stop-Process -Name ndClickWinTray
Stop-Process -Name NetDocuments.ndMail.Application
Stop-Process -Name ndOffice
Write-Host "ND processes closed successfully"

#Start all ND processes

if(Test-Path -Path "C:\Program Files (x86)\NetDocuments\ndClick"){
    Start-Process -FilePath ndClickWinTray -WorkingDirectory "C:\Program Files (x86)\NetDocuments\ndClick"
} else {
    Start-Process -FilePath ndClickWinTray -WorkingDirectory "C:\Program Files\NetDocuments\ndClick"
}
Start-Process -FilePath "NetDocuments.ndMail.Application.exe" -WorkingDirectory "C:\Program Files (x86)\NetDocuments\ndMail"
Start-Process -FilePath ndOffice -WorkingDirectory "C:\Program Files (x86)\NetDocuments\ndOffice"
Write-Host "ND processes started successfully"
