
#Instantiate error report variable
$err_report=@()

######Copying of MS Office template files from .v2 profile to .v2.V6 profile#####

#Username to perform operations on
$user = Read-Host -Prompt 'Enter username';

#Source for Microsoft Office templates (.v2 profile)
$tmplSource = "\\hgfile1\rprofiles7$\" + $user + ".V2\AppData\Roaming\Microsoft\Office"

#Destination for Microsoft Office templates (.V6 profile)
$tmplDestination = "\\hgfile1\rprofiles7$\" + $user + ".v2.V6\AppData\Roaming\Microsoft\Office"

try{
    Copy-Item $tmplSource -Destination $tmplDestination -Recurse -ErrorVariable err_report
}
catch {
    Write-Host "Please check MS Office template files if moved properly" -ForegroundColor DarkYellow -BackgroundColor Black
}


Write-Host "Copied templates..." -ForegroundColor Green;


######Copying of MS Office Signatures for Outlook#####

#Source for Microsoft signatures (.v2 profile)
$sigSource = "\\hgfile1\rprofiles7$\" + $user + ".V2\AppData\Roaming\Microsoft\Signatures"

#Destination for Microsoft signatures (.v2 profile)
$sigDestination = "\\hgfile1\rprofiles7$\" + $user + ".v2.V6\AppData\Roaming\Microsoft\"

try{
    Copy-Item $sigSource -Destination $sigDestination -Recurse -ErrorVariable err_report
}
catch {
    Write-Host "Please check MS Office signature files if moved properly" -ForegroundColor DarkYellow -BackgroundColor Black
}

Write-Host "Copied signatures..." -ForegroundColor Green;


#####Enable File Explorer Naviagtion Pane Settings and Show File Extensions#####
$key = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
Set-ItemProperty $key NavPaneExpandToCurrentFolder 1
Set-ItemProperty $key NavPaneShowAllFolders 1
Set-ItemProperty $key HideFileExt 0
Write-Host "Set File Explorer settings" -ForegroundColor Green

#####Disable auto logon#####
$key = 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon'
Set-ItemProperty $key AutoAdminLogon 0
Write-Host "Disabled AutoAdminLogon" -ForegroundColor Green

#####Delete Unattend files#####
$unattend = @('C:\Windows\Panther\unattend.xml', 'C:\Windows\Panther\Unattend\unattend.xml', 'C:\unattend.xml', 'C:\unattend-capture.xml')
$unattend | ForEach-Object {if(Test-Path "$PSItem"){ Remove-Item -Path "$PSItem"}}
Write-Host "Deleted Unattend files" -ForegroundColor Green

#Set-ExecutionPolicy Default -Force 

#Write-Host "Set Execution policy to Default" -ForegroundColor "Green" 

#####Enable Windows Features - .NET Framework 3.5#####

DISM /Online /Enable-Feature /FeatureName:NetFx3 /All /LimitAccess /Source:\\st-dfs\IT\Scripting\1903

#Write-Host "Type `$err_report to view error report" -ForegroundColor Red -BackgroundColor Black

$exit = Read-Host -Prompt 'Press enter to exit...';

Stop-Process -processname explorer
