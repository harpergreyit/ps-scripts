﻿#foreach($line in Get-Content .\permission-check.txt) {
#echo $line
#$line | Add-content -path '\\hgfile1\RDF$\mbajaman\Desktop\crusnak-report.csv'
#Get-MailboxFolderPermission $line | Select FolderName, User, AccessRights | 
#Export-Csv -Path '\\hgfile1\RDF$\mbajaman\Desktop\crusnak-report.csv' -Encoding ascii -NoTypeInformation -Append -Force
#}
##################################################################
#$user = 'mbajaman'
#
#$path = Get-MailboxFolderStatistics $user | Select FolderPath
#$newpath = $null
#
#$path.GetEnumerator() | ForEach-Object {
#if( $_.FolderPath -notmatch "(\/(.*?)\/)") {
#     $newpath = $newpath + "`n$user`:"+ $_.FolderPath
#    }
#}
#
#$newpath = $newpath.TrimStart("`n");
#$newpath = $newpath -replace "/","\"
#
#foreach($path in $newpath){
#    Get-MailboxFolderStatistics
#}
##################################################################

function RemoveDeletedUserPermissions($Mailbox) {

    $MBX = (Get-Mailbox $Mailbox -ea SilentlyContinue)

    if($MBX) {

        # Remove any permissions on the mailbox root for non-existent users
        $Root = $Mailbox + ":\"
        $RootPerm = (Get-MailboxFolderPermission $Root | ?{$_.User -like "NT:S-1-5-21*"})

        if($RootPerm) {
            $RootPerm | foreach {
                Write-Host $Root
                $RootUser = $_.User.DisplayName
                Write-Host $RootUser
               Remove-MailboxFolderPermission $Root -User $RootUser -Confirm:$False
            }
        }

        # Get a list of all other mailbox folders. Skips folders that usually cause errors or that don't matter.
        $MBXFolders = (Get-MailboxFolderStatistics $Mailbox | ?{($_.FolderPath -ne "/Top of Information Store") `
        -and ($_.FolderPath -ne "/Recoverable Items") `
        -and ($_.FolderPath -ne "/Deletions") `
        -and ($_.FolderPath -ne "/Purges") `
        -and ($_.FolderPath -ne "/Audits") `
        -and ($_.FolderPath -ne "/Calendar Logging") `
        -and ($_.FolderPath -ne "/Versions")})

        # For each folder in the mailbox, find and remove any permission entries for dead users.
        ForEach($Folder in $MBXFolders) {
            $FolderPath = $Mailbox + ":" + $Folder.FolderPath.Replace("/","\")
            $Perm = (Get-MailboxFolderPermission $FolderPath | ?{$_.User -like "NT:S-1-5-21*"})

            if ($Perm) {
                $FolderPath
                $Perm | foreach {
                    $User = $_.User.DisplayName
                    $User
                    Remove-MailboxFolderPermission $FolderPath -User $User -Confirm:$False
                }
            }
        }

    }

    
}