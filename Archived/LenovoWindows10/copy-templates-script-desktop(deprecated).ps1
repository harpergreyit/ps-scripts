#Admin rights
param([switch]$Elevated)
function Check-Admin {
$currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
$currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}
if ((Check-Admin) -eq $false)  {
if ($elevated)
{
# could not elevate, quit
}
 
else {
 
Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
}
exit
}

#####Command to remove XBOX#####
echo "Removing xbox package..."
Get-AppxPackage *xbox* | Remove-AppxPackage

#####Add registry key to turn off auto update for windows app#####
echo "Running registry file DisableAutoUpdateWindowsStore.reg..."
regedit.exe /s '\\hgdata1\db\Common\Scripts\DisableAutoUpdateWindowsStore.reg'

#####Turn of extra notification except All settings, Network and Location#####
#----------------------------------------------------------------------------#

#####Uninstall unwanted apps#####
Get-AppxPackage *skype* | Remove-AppxPackage
Get-AppxPackage *actipro* | Remove-AppxPackage
Get-AppxPackage *networkspeedtest* | Remove-AppxPackage
Get-AppxPackage *duolingo* | Remove-AppxPackage
Get-AppxPackage *pandora* | Remove-AppxPackage
Get-AppxPackage *freshpaint* | Remove-AppxPackage
Get-AppxPackage *officehub* | Remove-AppxPackage
Get-AppxPackage *onenote* | Remove-AppxPackage
Get-AppxPackage *photoshopexpress* | Remove-AppxPackage
Get-AppxPackage *eclipse* | Remove-AppxPackage
Get-AppxPackage *advertising* | Remove-AppxPackage
Get-AppxPackage *appinstaller* | Remove-AppxPackage
Get-AppxPackage *bing* | Remove-AppxPackage
Get-AppxPackage *sway* | Remove-AppxPackage
Get-AppxPackage *getstarted* | Remove-AppxPackage

##### Unpin all start menu tiles #####
function Pin-App {    param(
        [string]$appname,
        [switch]$unpin
    )
    try{
        if ($unpin.IsPresent){
            ((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | ?{$_.Name -eq $appname}).Verbs() | ?{$_.Name.replace('&','') -match 'Von "Start" lösen|Unpin from Start'} | %{$_.DoIt()}
            return "App '$appname' unpinned from Start"
        }else{
            ((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | ?{$_.Name -eq $appname}).Verbs() | ?{$_.Name.replace('&','') -match 'An "Start" anheften|Pin to Start'} | %{$_.DoIt()}
            return "App '$appname' pinned to Start"
        }
    }catch{
        echo "#####Error Unpinning App! '$appname' (Is the App-Name correct? Was the app already unpinned?)#####"
    }
}

Pin-App "Calendar" -unpin
Pin-App "Alarms & Clock" -unpin
Pin-App "Mail" -unpin
Pin-App "Get Office" -unpin
Pin-App "OneNote" -unpin
Pin-App "Microsoft Store" -unpin
Pin-App "Microsoft Edge" -unpin
Pin-App "News" -unpin
Pin-App "Skype" -unpin
Pin-App "Adobe Photoshop Express" -unpin
Pin-App "Weather" -unpin
Pin-App "Eclipse Manager" -unpin
Pin-App "Duolingo" -unpin
Pin-App "Xbox" -unpin
Pin-App "Movies & TV" -unpin
Pin-App "Groove Music" -unpin
Pin-App "Calculator" -unpin
Pin-App "Maps" -unpin
Pin-App "Photos" -unpin
Pin-App "Network Speed Test" -unpin
Pin-App "Sway" -unpin
Pin-App "Code Writer" -unpin
Pin-App "Remote Desktop" -unpin
Pin-App "Tips" -unpin
Pin-App "Translator" -unpin
Pin-App "Fresh Paint" -unpin


##### Install audio driver #####
Get-ChildItem "C:\Windows\System32\DriverStore\FileRepository\hdau*" -Recurse -Filter "*.inf" | ForEach-Object { PNPUtil.exe /add-driver $_.FullName /install }

######Copying of MS Office template files from .v2 profile to .v2.V6 profile#####

#Username to perform operations on
$user = Read-Host -Prompt 'Enter username: ';

#Source for Microsoft Office templates (.v2 profile)
$tmplSource = "\\hgfile1\rprofiles7$\" + $user + ".V2\AppData\Roaming\Microsoft\Office";

#Destination for Microsoft Office templates (.V6 profile)
$tmplDestination = "\\hgfile1\rprofiles7$\" + $user + ".v2.V6\AppData\Roaming\Microsoft\";

Copy-Item $tmplSource -Destination $tmplDestination -Recurse;

echo "Copied templates...";


######Copying of MS Office Signatures for Outlook#####

#Source for Microsoft signatures (.v2 profile)
$sigSource = "\\hgfile1\rprofiles7$\" + $user + ".V2\AppData\Roaming\Microsoft\Signatures";

#Destination for Microsoft signatures (.v2 profile)
$sigDestination = "\\hgfile1\rprofiles7$\" + $user + ".v2.V6\AppData\Roaming\Microsoft\";

Copy-Item $sigSource -Destination $sigDestination -Recurse;

echo "Copied signatures...";

#####Copying Adobe Acrobat and Reader plug-ins#####
echo "Copying Adobe plug-ins..."

#Plug-In source
$pluginSource = "\\hgnas1\IT\Repo\OpenText\AdobePlugin\"

#Plug-In destinations
$readerDestination = "C:\Program Files (x86)\Adobe\Reader 11.0\Reader\plug_ins"
$acrobatDestination = "C:\Program Files (x86)\Adobe\Acrobat 11.0\Acrobat\plug_ins"
$acrobatDestination2 = "C:\Program Files (x86)\Adobe\Acrobat 10.0\Acrobat\plug_ins"
$acrobatDestination3 = "C:\Program Files (x86)\Adobe\Acrobat DC\Acrobat\plug_ins"

#Test if path exists
if (Test-Path $readerDestination)
 {
  #then copy
  robocopy $pluginSource $readerDestination eDOCSReader.api /B /E /MT[:128];
 }

#Test if path exists
if (Test-Path $acrobatDestination)
 {
  #then copy
  robocopy $pluginSource $acrobatDestination eDOCSAcrobat.api /B /E /MT[:128];
 }

 #Test if path exists
if (Test-Path $acrobatDestination2)
 {
  #then copy
  robocopy $pluginSource $acrobatDestination2 eDOCSAcrobat.api /B /E /MT[:128];
 }

 if (Test-Path $acrobatDestination3)
 {
  #then copy
  robocopy $pluginSource $acrobatDestination3 eDOCSAcrobat.api /B /E /MT[:128];
 }

#####Enable File Explorer Naviagtion Pane Settings#####
$key = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
Set-ItemProperty $key NavPaneExpandToCurrentFolder 1
Set-ItemProperty $key NavPaneShowAllFolders 1
Stop-Process -processname explorer


$exit = Read-Host -Prompt 'Press enter to exit...';
