#####Enable File Explorer Naviagtion Pane Settings and Show File Extensions#####
$key = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
Set-ItemProperty $key NavPaneExpandToCurrentFolder 1
Set-ItemProperty $key NavPaneShowAllFolders 1
Set-ItemProperty $key HideFileExt 0

#####Delete Unattend files#####
Remove-Item -Path "C:\Windows\Panther\unattend.xml"
Remove-Item -Path "C:\Windows\Panther\Unattend\unattend.xml"
Remove-Item -Path "C:\unattend.xml"

