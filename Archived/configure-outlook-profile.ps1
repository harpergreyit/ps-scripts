#Reference: https://stackoverflow.com/questions/5648931/test-if-registry-value-exists


Function ZCE-Config {
    param(
        [Alias("PSPath")]
        [Parameter(Position = 0, Mandatory = $true, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
        [String]$Path
        ,
        [Parameter(Position = 1, Mandatory = $true)]
        [String]$Name
    ) 

    process {
        if (Test-Path $Path) {
            $Key = Get-Item -LiteralPath $Path
            if ($Key.GetValue($Name, $null) -ne $null) {
                Write-Host "Showing key information" -ForegroundColor Green
                Get-ItemProperty $Path $Name
                $KeyValue = Get-ItemProperty $Path $Name | Select $Name

                if($KeyValue.$name -eq "Exchange"){
                    Write-Host "Changing DefaultProfile key value at $Path" -ForegroundColor Green
                    Set-ItemProperty $Path $Name -Value "Outlook" #Changes value for DefaultProfile

                    Write-Host "`nCreating new subkey called Outlook under $Path" -ForegroundColor Green
                    New-Item $Path -Name Outlook #Creates new subkey

                    # if(Test-Path $Path\Exchange){
                    #     Write-Host "Removing Exchange profile" -ForegroundColor Green
                    #     Remove-Item $Path\Exchange
                    # }
                }
            } else {
                Add-KeyProperty $Path $Name             
            }
        } else {
            Write-Host "`nCreating Key at: $Path" -ForegroundColor Green
            New-Item $Path #Creates new subkey
            Add-KeyProperty $Path $Name
        }
    }
}

Function Add-KeyProperty{
    param(
        [Alias("PSPath")]
        [Parameter(Position = 0, Mandatory = $true, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
        [String]$Path
        ,
        [Parameter(Position = 1, Mandatory = $true)]
        [String]$Name
    )

    process {
        if($Name -eq "DefaultProfile"){
            Write-Host "Creating DefaultProfile key and value at $Path" -ForegroundColor Green
            New-ItemProperty $Path $Name -Value "Outlook"

            Write-Host "`nCreating new subkey called Outlook under $Path" -ForegroundColor Green
            New-Item $Path -Name Outlook #Creates new subkey

            Write-Host "Removing Exchange profile" -ForegroundColor Green
            Remove-Item $Path\Exchange
        }

        if($Name -eq "ZeroConfigExchange"){
            Write-Host "Creating key property value`nPath:$Path`nName:$Name`nValue:1`nType:DWORD" -ForegroundColor Green
            New-ItemProperty $Path $Name -PropertyType "DWORD" -Value "1" #Creates new value under the subkey created above
        }
    }
}

#Check if default profile is set to "Exchange", if profile exists and is set to Exchange, change default profile and create a new subkey with the new profile name
ZCE-Config "HKCU:\Software\Microsoft\Windows NT\CurrentVersion\Windows Messaging Subsystem\Profiles" "DefaultProfile"

#Check if AutoDiscover subkey exists, if it doesn't exist, creates and configures it for ZeroConfigExchange
ZCE-Config "HKCU:\Software\Microsoft\Office\14.0\Outlook\AutoDiscover" "ZeroConfigExchange"


#Microsoft VSTO removal
#Computer\HKEY_CURRENT_USER\Software\Microsoft\Office\Excel\Addins\DMExcel2010
#Computer\HKEY_CURRENT_USER\Software\Microsoft\Office\PowerPoint\Addins\DMPowerPoint2010
#Computer\HKEY_CURRENT_USER\Software\Microsoft\Office\Word\Addins\DMWord2010


