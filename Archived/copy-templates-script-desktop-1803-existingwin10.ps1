
#Instantiate error report variable
$err_report=@()

##### Unpin all start menu tiles #####
function Pin-App {    param(
        [string]$appname,
        [switch]$unpin
    )
    try{
        if ($unpin.IsPresent){
            ((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | ?{$_.Name -eq $appname}).Verbs() | ?{$_.Name.replace('&','') -match 'Von "Start" lösen|Unpin from Start'} | %{$_.DoIt()}
            return "App '$appname' unpinned from Start"
        }else{
            ((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | ?{$_.Name -eq $appname}).Verbs() | ?{$_.Name.replace('&','') -match 'An "Start" anheften|Pin to Start'} | %{$_.DoIt()}
            return "App '$appname' pinned to Start"
        }
    }catch{
        echo "#####Error Unpinning App! '$appname' (Is the App-Name correct? Was the app already unpinned?)#####"
    }
}

Pin-App "Microsoft Store" -unpin -ErrorAction SilentlyContinue
Pin-App "Microsoft Edge" -unpin -ErrorAction SilentlyContinue
Pin-App "Photos" -unpin -ErrorAction SilentlyContinue

#Pin Installed Apps
Invoke-Expression -Command \\hgfile1\IT\Scripting\Pin-Office-Apps.ps1

#####Set correct Time Zone#####
[string]$TimeZone = "Pacific Standard Time"
try {
   Set-TimeZone -Id $TimeZone
   Write-Host "Success: the time zone $TimeZone has been set"
} Catch {
   Write-Host "Error: the time zone $TimeZone does not exist!"
}

#####Enable File Explorer Naviagtion Pane Settings and Show File Extensions#####
$key = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
Set-ItemProperty $key NavPaneExpandToCurrentFolder 1
Set-ItemProperty $key NavPaneShowAllFolders 1
Set-ItemProperty $key HideFileExt 0
Write-Host "Set File Explorer settings" -ForegroundColor Green

#####Disable auto logon#####
$key = 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon'
Set-ItemProperty $key AutoAdminLogon 0
Write-Host "Disabled AutoAdminLogon" -ForegroundColor Green

#####Delete Unattend files#####
$unattend = @('C:\Windows\Panther\unattend.xml', 'C:\Windows\Panther\Unattend\unattend.xml', 'C:\unattend.xml', 'C:\unattend-capture.xml')
$unattend | ForEach-Object {if(Test-Path "$PSItem"){ Remove-Item -Path "$PSItem"}}
Write-Host "Deleted Unattend files" -ForegroundColor Green

Set-ExecutionPolicy Default -Force 

Write-Host "Set Execution policy to Default" -ForegroundColor "Green" 

#Write-Host "Type `$err_report to view error report" -ForegroundColor Red -BackgroundColor Black

$exit = Read-Host -Prompt 'Press enter to exit...';

Stop-Process -processname explorer
