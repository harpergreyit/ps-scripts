Param([switch]$Bitlocker, [switch]$Office, [switch]$Adobe, [switch]$All, [switch]$Elevated)

#Admin rights
function Check-Admin {
$currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
$currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}
if ((Check-Admin) -eq $false)  {
if ($elevated)
{
# could not elevate, quit
}
 
else {
 
Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
}
exit
}

function bitlocker_config {
    #Convert Volume type to boot using UEFI
    mbr2gpt /convert /allowFullOS

    #Rename file to reset Bitlocker settings.
    Rename-Item -Path "C:\Windows\System32\Recovery\ReAgent.xml" -NewName "ReAgent.xml.old"
}

function office_config{
    #####Disable auto logon#####
    $key = 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon'
    Set-ItemProperty $key AutoAdminLogon 0
    echo 'Disabled AutoAdminLogon'

    ######Copying of MS Office template files from .v2 profile to .v2.V6 profile#####

    #Username to perform operations on
    $user = Read-Host -Prompt 'Enter username: ';

    #Source for Microsoft Office templates (.v2 profile)
    $tmplSource = "\\hgfile1\rprofiles7$\" + $user + ".V2\AppData\Roaming\Microsoft\Office";

    #Destination for Microsoft Office templates (.V6 profile)
    $tmplDestination = "\\hgfile1\rprofiles7$\" + $user + ".v2.V6\AppData\Roaming\Microsoft\";

    Copy-Item $tmplSource -Destination $tmplDestination -Recurse;

    echo "Copied templates...";

    ######Copying of MS Office Signatures for Outlook#####

    #Source for Microsoft signatures (.v2 profile)
    $sigSource = "\\hgfile1\rprofiles7$\" + $user + ".V2\AppData\Roaming\Microsoft\Signatures";

    #Destination for Microsoft signatures (.v2 profile)
    $sigDestination = "\\hgfile1\rprofiles7$\" + $user + ".v2.V6\AppData\Roaming\Microsoft\";

    Copy-Item $sigSource -Destination $sigDestination -Recurse;

    echo "Copied signatures...";
}

function adobe_config{
    #####Copying Adobe Acrobat and Reader plug-ins#####
    echo "Copying Adobe plug-ins..."

    #Plug-In source
    $pluginSource = "\\hgnas1\IT\Repo\OpenText\AdobePlugin\"

    #Plug-In destinations
    $readerDestination = "C:\Program Files (x86)\Adobe\Reader 11.0\Reader\plug_ins"
    $acrobatDestination = "C:\Program Files (x86)\Adobe\Acrobat 11.0\Acrobat\plug_ins"
    $acrobatDestination2 = "C:\Program Files (x86)\Adobe\Acrobat 10.0\Acrobat\plug_ins"
    $acrobatDestination3 = "C:\Program Files (x86)\Adobe\Acrobat DC\Acrobat\plug_ins"

    #Test if path exists
    if (Test-Path $readerDestination)
    {
        #then copy
        robocopy $pluginSource $readerDestination eDOCSReader.api /B /E /MT[:128];
    }

    #Test if path exists
    if (Test-Path $acrobatDestination)
    {
        #then copy
        robocopy $pluginSource $acrobatDestination eDOCSAcrobat.api /B /E /MT[:128];
    }

    #Test if path exists
    if (Test-Path $acrobatDestination2)
    {
        #then copy
        robocopy $pluginSource $acrobatDestination2 eDOCSAcrobat.api /B /E /MT[:128];
    }

    if (Test-Path $acrobatDestination3)
    {
        #then copy
        robocopy $pluginSource $acrobatDestination3 eDOCSAcrobat.api /B /E /MT[:128];
    }

    #####Enable File Explorer Naviagtion Pane Settings#####
    $key = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
    Set-ItemProperty $key NavPaneExpandToCurrentFolder 1
    Set-ItemProperty $key NavPaneShowAllFolders 1
    Stop-Process -processname explorer
}

if ($Bitlocker){
    bitlocker_config
    $result = 'Success'
}

if ($Office){
    office_config
    $result = 'Success'
}

if ($Adobe){
    adobe_config
    $result = 'Success'
}

if ($All){
    bitlocker_config
    office_config
    adobe_config
    $result = 'Success'
}

if ($result == 'Success') {
    Set-ExecutionPolicy Default
}