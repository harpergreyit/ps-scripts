while(1)
{

    Write-Host "Running status check" -ForegroundColor "Green" 

    Get-Date;
	Get-Service -ComputerName dm1 -Name "Open Text eDOCS Email Filing Exchange Connector" | Select Name, MachineName, Status;
	Get-Service -ComputerName dm1 -Name "Open Text eDOCS Email Filing Server Marker" | Select Name, MachineName, Status;
	Get-Service -ComputerName dm1 -Name "Open Text eDOCS Email Filing Service" | Select Name, MachineName, Status

    Write-Host "Check will run again in 1 hour" -ForegroundColor "Green" 

	Start-Sleep -Seconds 3600

    Write

}