
#Instantiate error report variable
$err_report=@()

##### Unpin all start menu tiles #####
function Pin-App {    param(
        [string]$appname,
        [switch]$unpin
    )
    try{
        if ($unpin.IsPresent){
            ((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | ?{$_.Name -eq $appname}).Verbs() | ?{$_.Name.replace('&','') -match 'Von "Start" lösen|Unpin from Start'} | %{$_.DoIt()}
            return "App '$appname' unpinned from Start"
        }else{
            ((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | ?{$_.Name -eq $appname}).Verbs() | ?{$_.Name.replace('&','') -match 'An "Start" anheften|Pin to Start'} | %{$_.DoIt()}
            return "App '$appname' pinned to Start"
        }
    }catch{
        echo "#####Error Unpinning App! '$appname' (Is the App-Name correct? Was the app already unpinned?)#####"
    }
}

Pin-App "Microsoft Store" -unpin -ErrorAction SilentlyContinue
Pin-App "Microsoft Edge" -unpin -ErrorAction SilentlyContinue
Pin-App "Photos" -unpin -ErrorAction SilentlyContinue

######Copying of MS Office template files from .v2 profile to .v2.V6 profile#####

#Username to perform operations on
$user = Read-Host -Prompt 'Enter username: ';

#Source for Microsoft Office templates (.v2 profile)
$tmplSource = "\\hgfile1\rprofiles7$\" + $user + ".V2\AppData\Roaming\Microsoft\Office"

#Destination for Microsoft Office templates (.V6 profile)
$tmplDestination = "\\hgfile1\rprofiles7$\" + $user + ".v2.V6\AppData\Roaming\Microsoft\Office"

try{
    Copy-Item $tmplSource -Destination $tmplDestination -Recurse -ErrorVariable err_report
}
catch {
    Write-Host "Please check MS Office template files if moved properly" -ForegroundColor DarkYellow -BackgroundColor Black
}


Write-Host "Copied templates..." -ForegroundColor Green;


######Copying of MS Office Signatures for Outlook#####

#Source for Microsoft signatures (.v2 profile)
$sigSource = "\\hgfile1\rprofiles7$\" + $user + ".V2\AppData\Roaming\Microsoft\Signatures"

#Destination for Microsoft signatures (.v2 profile)
$sigDestination = "\\hgfile1\rprofiles7$\" + $user + ".v2.V6\AppData\Roaming\Microsoft\"

try{
    Copy-Item $sigSource -Destination $sigDestination -Recurse -ErrorVariable err_report
}
catch {
    Write-Host "Please check MS Office signature files if moved properly" -ForegroundColor DarkYellow -BackgroundColor Black
}

Write-Host "Copied signatures..." -ForegroundColor Green;

 #####Copying Adobe Acrobat and Reader plug-ins#####
#Plug-In source
$pluginSource = "\\st-dfs\IT\Repo\OpenText\AdobePlugin\"

#Plug-In destinations
$readerDestination = "C:\Program Files (x86)\Adobe\Reader 11.0\Reader\plug_ins"
$acrobatDestination = "C:\Program Files (x86)\Adobe\Acrobat 11.0\Acrobat\plug_ins"
$acrobatDestination2 = "C:\Program Files (x86)\Adobe\Acrobat 10.0\Acrobat\plug_ins"
$acrobatDestination3 = "C:\Program Files (x86)\Adobe\Acrobat DC\Acrobat\plug_ins"

#Test if path exists
if (Test-Path $readerDestination)
 {
  #then copy
  robocopy $pluginSource $readerDestination eDOCSReader.api /NJH /NJS /NDL /NC /NS /B /E /MT[:128];
 }

#Test if path exists
if (Test-Path $acrobatDestination)
 {
  #then copy
  robocopy $pluginSource $acrobatDestination eDOCSAcrobat.api /NJH /NJS /NDL /NC /NS /B /E /MT[:128];
 }

 #Test if path exists
if (Test-Path $acrobatDestination2)
 {
  #then copy
  robocopy $pluginSource $acrobatDestination2 eDOCSAcrobat.api /NJH /NJS /NDL /NC /NS /B /E /MT[:128];
 }

 if (Test-Path $acrobatDestination3)
 {
  #then copy
  robocopy $pluginSource $acrobatDestination3 eDOCSAcrobat.api /NJH /NJS /NDL /NC /NS /B /E /MT[:128];
 }

Write-Host "Copied Adobe plug-ins..." -ForegroundColor Green

#####Enable File Explorer Naviagtion Pane Settings and Show File Extensions#####
$key = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
Set-ItemProperty $key NavPaneExpandToCurrentFolder 1
Set-ItemProperty $key NavPaneShowAllFolders 1
Set-ItemProperty $key HideFileExt 0
Write-Host "Set File Explorer settings" -ForegroundColor Green

#####Disable auto logon#####
$key = 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon'
Set-ItemProperty $key AutoAdminLogon 0
Write-Host "Disabled AutoAdminLogon" -ForegroundColor Green

#####Delete Unattend files#####
Remove-Item -Path "C:\Windows\Panther\unattend.xml"
Remove-Item -Path "C:\Windows\Panther\Unattend\unattend.xml"
Remove-Item -Path "C:\unattend.xml"
Write-Host "Deleted Unattend files" -ForegroundColor Green

#####Registry edit for eDOCS DM(footer autopopulate)#####
reg import "\\st-dfs\IT\Repo\OpenText\RegEdits\Local User\Word Options.reg"
Write-Host "Imported Word Options.reg" -ForegroundColor Green

Set-ExecutionPolicy Default -Force 

Write-Host "Set Execution policy to Default" -ForegroundColor "Green" 

#Write-Host "Type `$err_report to view error report" -ForegroundColor Red -BackgroundColor Black

$exit = Read-Host -Prompt 'Press enter to exit...';

Stop-Process -processname explorer
