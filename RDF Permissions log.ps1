﻿$FilePath = "C:\Users\Username-list.csv"
$usernames = Import-CSV $FilePath -Delimiter ","

$Path = "\\hgfile1\RDF$\"
$LogPath = "C:\Users"

Foreach ($username in $usernames) {
    $DomainUser = "HARPERGREY\" + $username.Name
    $DomainUser
    $UserPath = $Path + $username.Name
    Get-ChildItem $UserPath -Force -Recurse | Select Name,Directory,@{Name="Owner";Expression={(Get-ACL $_.Fullname).Owner}} | 
    Where Owner -ne $DomainUser |
    Export-Csv $LogPath\FileFolderOwner.csv -NoTypeInformation -Append
}


