function Pin-App { param(
[string]$appname,
[switch]$unpin
)
try{
if ($unpin.IsPresent){
((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | ?{$_.Name -eq $appname}).Verbs() | ?{$_.Name.replace('&','') -match 'From "Start" UnPin|Unpin from Start'} | %{$_.DoIt()}
return "App '$appname' unpinned from Start"
}else{
((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | ?{$_.Name -eq $appname}).Verbs() | ?{$_.Name.replace('&','') -match 'To "Start" Pin|Pin to Start'} | %{$_.DoIt()}
return "App '$appname' pinned to Start"
}
}catch{
Write-Error "Error Pinning/Unpinning App! (App-Name correct?)"
}
}

Pin-App "Outlook" -pin
Pin-App "Word" -pin
Pin-App "Excel" -pin
Pin-App "Powerpoint" -pin
Pin-App "OneNote 2016" -pin
Pin-App "Cisco Jabber" -pin
Pin-App "Citrix Workspace" -pin
Pin-App "CDTracker" -pin
Pin-App "Adobe Reader XI" -pin
Pin-App "Google Chrome" -pin
Pin-App "Internet Explorer" -pin
Pin-App "Snipping Tool" -pin

