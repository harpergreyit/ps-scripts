$OUpath = 'ou=General,ou=Users,ou=Win7_Service_Area,dc=harpergrey,dc=internal'
$ExportPath = 'c:\users_in_win7_ou.csv'
Get-ADUser -Filter * -SearchBase $OUpath | Where { $_.Enabled -eq $True} | Select-object SurName, Name, UserPrincipalName| Export-Csv -NoType $ExportPath